// pints and cups are imperial
const inLitres = (amount, unit) => {
  amount = Number(amount);
  switch(unit) {
  case ("dl"):
    return amount / 10;
  case ("l"):
    return amount;
  case ("oz"):
    return amount / 35.195;
  case ("cup"):
    return amount / 3.52;
  case ("pint"):
    return amount / 1.76;
  default:
    return NaN;
  }
};

const litresInOtherUnits = (amount, unit) => {
  amount = Number(amount);
  switch(unit) {
  case ("dl"):
    return amount * 10;
  case ("l"):
    return amount;
  case ("oz"):
    return amount * 35.195;
  case ("cup"):
    return amount * 3.52;
  case ("pint"):
    return amount * 1.76;
  default:
    return NaN;
  }
};

export const convert = (amount, unitFrom, unitTo) => litresInOtherUnits(inLitres(amount, unitFrom), unitTo);

const args = process.argv.slice(2);

if (args.length < 3) {
  console.log("You need to provide 3 arguments.");
} else {
  const result = convert(...args);
  if (isNaN(result)) {
    console.log("Allowed unit types are 'dl', 'l', 'oz', 'cup' and 'pint'.");
  } else {
    console.log(result.toFixed(3));
  }
}